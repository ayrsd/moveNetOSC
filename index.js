/**
 * Forked from https://gitlab.com/tomasciccola/moveNetTest
 */

const http = require('http')
const fs = require('fs')
const path = require('path')
const { Server } = require("socket.io")

const { Client } = require('node-osc')
const client = new Client('127.0.0.1', 9001)

const midi = require('midi')
const {scenes} = require("./scenes.js") // HUMAN : Set your scenes here

let MIDI_ENABLED = true // Want to changes scenes using MIDI?
let MIDI_FEEDBACK_ENABLED = true // Want a flashy feedback from you MIDI device ?
let MIDI_INPUT_PORT = null //Set input port or it will connect to the last one
let MIDI_OUTPUT_PORT = null //Set output port or it will connect to the last one
const MIDI_NOTES_LISTENING = [ 72, 74, 76, 77 ] // What notes should I'll listen to ?
const MIDI_MESSAGE_STATUS = 148 //learnt it from console.log()

const PORT = process.env.PORT || 5000
const ROOT = 'public'

const N_POSES = 2
const PARTS = ['left_wrist','right_wrist', 'nose']

const MIDI_input = new midi.Input();
const MIDI_output = new midi.Output();

let cantMensajes = 0

// setInterval(_ => {
//   console.log( `Puntos por segundo: ${cantMensajes}` )
//   cantMensajes = 0
// } , 1000)

const onRequest = (req,res) => {
  console.log(req.method, req.url)

  if(req.url === '/'){
    return res.end(fs.readFileSync(path.join(ROOT, 'index.html')))
  }

  if(req.url === '/index.js'){
    return res.end(fs.readFileSync(path.join(ROOT, 'index.js')))
  }

  if(req.url === '/lib/tfjs-core.js'){
    return res.end(fs.readFileSync(path.join(ROOT, 'lib/tfjs-core.js')))
  }

  if(req.url === '/lib/tfjs-converter.js'){
    return res.end(fs.readFileSync(path.join(ROOT, 'lib/tfjs-converter.js')))
  }

  if(req.url === '/lib/tfjs-backend-webgl.js'){
    return res.end(fs.readFileSync(path.join(ROOT, 'lib/tfjs-backend-webgl.js')))
  }

  if(req.url === '/lib/pose-detection.js'){
    return res.end(fs.readFileSync(path.join(ROOT, 'lib/pose-detection.js')))
  }

}

const sendData = (coordinateName) => (data) => {
  cantMensajes++;
  
  if ( coordinateName in currentScene ) {
    let coordinateParams = currentScene[coordinateName]
    
    let name = coordinateName;
    if ( COORDINATE_PARAMS_NAME in coordinateParams ) {
      name = coordinateParams[COORDINATE_PARAMS_NAME]
    }

    let bundled_xy = true
    if ( COORDINATE_PARAMS_BUNDLED_XY in coordinateParams ) {
      bundled_xy = coordinateParams[COORDINATE_PARAMS_BUNDLED_XY]
    }

    ////////////////

    if ( bundled_xy ) {
      const dString = `/${name}/play/xy`
      client.send(dString,data.x,data.y)
    } else {
      let dString = `/${name}/play/x`
      client.send(dString,data.x)

      dString = `/${name}/play/y`
      client.send(dString,data.y)
    }
  } else {
    if ( SEND_ALL ) {
      const dString = `/${coordinateName}/play/xy`
      // console.log('on', coordinateName, 'sending!', dString, data.x, data.y)
      client.send(dString,data.x,data.y)
    }
  }
}

const setCurrentScene = (sceneName, feedback = true) => {
  if ( sceneName in scenes ) {
    console.log( `Changing to scene: "${sceneName}"` )
    currentSceneName = sceneName
    currentScene = scenes[sceneName]

    if ( feedback ) feedbackCurrentScene()

  } else {
    console.log( `Error: no hay una escena llamada "${sceneName}"` )
  }
}
const feedbackCurrentScene = () => {
  //MIDI feedback
  if ( MIDI_ENABLED && MIDI_FEEDBACK_ENABLED ) {
    //Turn off everything
    for ( midi_note of MIDI_NOTES_LISTENING ) {
      MIDI_output.sendMessage([MIDI_MESSAGE_STATUS, midi_note, 0]);
    }
    
    //Turn on selected
    let scenes_names = Object.keys(scenes)
    let index = scenes_names.indexOf( currentSceneName )

    if ( index != -1 && index < MIDI_NOTES_LISTENING.length ) {
      MIDI_output.sendMessage([MIDI_MESSAGE_STATUS, MIDI_NOTES_LISTENING[index], 127]);
    }
  }

  //Web feedback
  if ( webSocket && webSocket.connected ) {
    webSocket.emit("setCurrentScene", currentSceneName)
  }
}

MIDI_input.on('message', (deltaTime, message) => {
  // The message is an array of numbers corresponding to the MIDI bytes:
  //   [status, data1, data2]
  // https://www.cs.cf.ac.uk/Dave/Multimedia/node158.html has some helpful
  // information interpreting the messages.
  // console.log(`m: ${message} d: ${deltaTime}`);
  let message_status = message[0]
  let message_note = message[1]
  let message_velocity = message[2]

  if ( message_status == MIDI_MESSAGE_STATUS && message_velocity == 127 ) {
    let index = MIDI_NOTES_LISTENING.indexOf( message_note )
    let scenes_names = Object.keys(scenes)
    
    if ( index != -1 && index < scenes_names.length ) {
      setCurrentScene( scenes_names[index] )
    }
  }  
});

const displayMIDIConnectionData = () => {
  console.log("MIDI INPUT")
    
  for ( let i = 0 ; i < MIDI_input.getPortCount() ; i++ ) {
    let connected = i == MIDI_INPUT_PORT ? "<---------- [CONNECTED]" : ""
    console.log(`${i}. ${MIDI_input.getPortName(i)} ${connected}`)
  }
  console.log()


  console.log("MIDI OUTPUT")
    
  for ( let i = 0 ; i < MIDI_output.getPortCount() ; i++ ) {
    let connected = i == MIDI_OUTPUT_PORT ? "<---------- [CONNECTED]" : ""
    console.log(`${i}. ${MIDI_output.getPortName(i)} ${connected}`)
  }
  console.log()

  console.log()
}

////////////////
// MAIN
/////////////////
const server = http.createServer(onRequest)
const io = new Server(server);

const SEND_ALL = false
const COORDINATE_PARAMS_NAME = "name"
const COORDINATE_PARAMS_BUNDLED_XY = "bundled_xy"

let currentScene = null
let currentSceneName = null
if ( scenes ) {
  let scenes_names = Object.keys(scenes)
  if ( scenes_names ) {
    setCurrentScene(scenes_names[0], false)
  } else {
    console.info("No scenes found.")
  }
} else {
  console.info("No scenes found.")
}

if ( MIDI_ENABLED ) {
  if ( MIDI_input.getPortCount() > 1 ) { //midi thru is the first one
    if ( MIDI_INPUT_PORT == null ) {
      MIDI_INPUT_PORT = MIDI_input.getPortCount() - 1
    }
  } else {
    MIDI_ENABLED = false
    console.log("MIDI is enabled but no MIDI input found")
  }

  if ( MIDI_FEEDBACK_ENABLED && MIDI_output.getPortCount() > 0 ) {
    if ( MIDI_OUTPUT_PORT == null ) {
      MIDI_OUTPUT_PORT = MIDI_output.getPortCount() - 1
    }
  } else {
    MIDI_FEEDBACK_ENABLED = false
    console.log("MIDI and MIDI_FEEDBACK are enabled but no MIDI output found")
  }

  if ( MIDI_ENABLED ) {
    displayMIDIConnectionData()
    MIDI_input.openPort(MIDI_INPUT_PORT);

    if ( MIDI_FEEDBACK_ENABLED ) {
      MIDI_output.openPort(MIDI_OUTPUT_PORT);
    }
  }
}

let webSocket = null

io.on('connection', socket => {
  console.log('Web socket connection established :)')
  webSocket = socket

  for(let i = 0; i < N_POSES; i++){
    for(let j = 0; j < PARTS.length; j++){
      const str = `pose_${i}_${PARTS[j]}`
      socket.on(str, sendData(str))
    }
  }

  socket.on("setScene", setCurrentScene)
  socket.emit("listScenes", Object.keys(scenes) )
  feedbackCurrentScene()
})

server.listen(PORT, () => console.log(`Listening port: ${PORT}. \nPsst go here: http://localhost:5000/\n`))
