// https://storage.googleapis.com/tfjs-models/demos/pose-detection/index.html?model=movenet

const DETECT_POSE_RATE = 0 //en ms, 0 es lo más rápido que pueda
/*
  20 : 45 por segundo
  50 : 27 por segundo
*/

const video = document.querySelector('video')
const video_canvas = document.querySelector('#video_canvas') //document.createElement('canvas')
const video_canvas_ctx = video_canvas.getContext("2d")

const canvas = document.querySelector('#canvas')
const ctx = canvas.getContext('2d')
const socket = io()

// moveNet || blazePose
const MODEL = 'moveNet'
const parts = ['nose', 'left_wrist', 'right_wrist']
let currentStream
let detector

let frames = 0
let fps = 0
let startTime = performance.now()

const calculateFPSNormal = () => {

	var t = performance.now();
	var dt = t - startTime;

	// if elapsed time is greater than 1s
	if( dt > 1000 ) {
		// calculate the frames drawn over the period of time
		fps = frames * 1000 / dt;
		// and restart the values
		frames = 0;
		startTime = t;
	}
	frames++;
}

const throttle = (callback, time) => {
  if (throttleTimer) return;

  throttleTimer = true;

  setTimeout(() => {
      callback();
      throttleTimer = false;
  }, time);
}
const map_value = (val, in_min, in_max, out_min, out_max) =>  {
  return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
}

const stop = stream => stream.getTracks().forEach(tr => tr.stop())

// list available cams on select tag
const getCams = async _ => {
  const devices = await navigator.mediaDevices.enumerateDevices()
  return devices.filter(dev => dev.kind === "videoinput")
}

// load selected option cam
const loadCam = async cam =>{
  if(typeof currentStream !== 'undefined'){
    stop(currentStream)
  }

  const videoConstrains = {
    deviceId: {exact: cam.value},
    width: 640,
    height: 480, //360
  }

  if (navigator.mediaDevices.getUserMedia) {
    try {
      const stream = await navigator.mediaDevices
        .getUserMedia({video:videoConstrains, audio:false})
      video.srcObject = stream;
      video.play()
    }catch(e){
      console.log("Something went wrong!");
      console.log(e)
    }

  }
}

// load model and detector and start detecting
const loadModel = async () => {
  // config for blazePose
  let detectorConfig
  if(MODEL === 'blazePose'){
    detectorConfig = {
      runtime: 'tfjs',
      enableSmoothing: true,
      modelType: 'full'
    }
  }else if(MODEL === 'moveNet'){
    // config for moveNet 
    detectorConfig = {
      modelType: poseDetection.movenet.modelType.MULTIPOSE_LIGHTNING, //MULTIPOSE_LIGHTNING, SINGLEPOSE_LIGHTNING, SINGLEPOSE_THUNDER
      enableSmoothing: true,
      enableTracking: true, // sólo para multipose
      trackerType: poseDetection.TrackerType.BoundingBox
    }
  }
  let model = MODEL === 'moveNet' 
    ? poseDetection.SupportedModels.MoveNet 
    : poseDetection.SupportedModels.BlazePose

  detector = await poseDetection.createDetector(model, detectorConfig);
  detectPoses()
}

const sendPose = (poses,idx) =>{
  let canvas_ctx = ctx
  if ( rectMap.is_set() ) {
    canvas_ctx = video_canvas_ctx
  }

  
  for (let i = 0 ; i < poses[idx].keypoints.length; i++){
    for(let j = 0; j < parts.length; j ++){
      if(parts[j] === poses[idx].keypoints[i].name){
        const data = poses[idx].keypoints[i]
        const dataStr = `pose_${idx}_${data.name}`
  
        //draw
        canvas_ctx.fillStyle = "#FF0000";
        canvas_ctx.fillRect(data.x,data.y,7,7);
        
        //send
        if ( !rectMap.is_set() ) {
          data.x = map_value(data.x, 0, canvas.width, 0, 1)
          data.y = map_value(data.y, 0, canvas.height, 0, 1)
        } else {
          let rect_width = rectMap.bottomRight.x - rectMap.topLeft.x
          let rect_height = rectMap.bottomRight.y - rectMap.topLeft.y

          data.x = map_value(data.x, 0, rect_width, 0, 1)
          data.y = map_value(data.y, 0, rect_height, 0, 1)
        }
        socket.emit(dataStr,data)
      }
    }
  }
}

socket.on("listScenes", (scenes) => {
  let ulScenes = document.querySelector("#ulScenes")
  ulScenes.innerHTML = ""

  for ( let i in scenes ) {
    let scene = scenes[i]
    let li = document.createElement("li")
    let button = document.createElement("button")

    button.innerText = scene;
    button.addEventListener("click", function() {
      socket.emit("setScene", this.innerText)
    })
    if ( i == 0 ) {
      button.classList.add("active");
      // socket.emit("setScene", button.innerText)
    }

    li.appendChild(button)
    ulScenes.appendChild(li)
  }
})

socket.on("setCurrentScene", (scene_name) => {
  let allButtons = document.querySelectorAll("#ulScenes button")
  for ( button of allButtons ) {
    if ( button.innerText == scene_name ) {
      button.classList.add("active")
    } else {
      button.classList.remove("active")
    }
  }
})

// loop to detect poses
const detectPoses = async ()=>{

  calculateFPSNormal()
  let txtFps = document.getElementById("fps")
  txtFps.innerText = Math.floor(fps)

  let video_source = video
  if ( rectMap.is_set() ) {
    video_source = video_canvas
  }

  //pose estimation
  let poses;
  if(MODEL === 'blazePose'){
    const estimationConfig = {flipHorizontal:true}
    const timestamp = performance.now()
    poses = await detector.estimatePoses(video_source, estimationConfig, timestamp)
  }else if(MODEL === 'moveNet'){
    poses = await detector.estimatePoses(video_source)
  }
  
  if(poses[0]){
    sendPose(poses,0)
  }

  if(poses[1]){
    sendPose(poses,1)
  }

  if( DETECT_POSE_RATE > 0 ){
    setTimeout(_ => detectPoses(), DETECT_POSE_RATE)//cap at N ms per frame
  }else{
    requestAnimationFrame(detectPoses)
  }
}

///////////////
// Draw rectangle for mapping
//////////////
class Point {
  constructor() {
    this.reset()
  }

  is_set() {
    return this.x != -66;
  }

  reset() {
    this.x = -66
    this.y = -66
  }
}
//nasty workaround, 0,0 wasn't really 0,0
function fixCoordinate(x,y) {
  let newPoint = new Point()
  newPoint.x = x - 8
  newPoint.y = y - 55
  return newPoint
}
let rectMap = {
  topLeft : new Point(),
  bottomRight : new Point(),

  is_set : () => {
    return rectMap.topLeft.is_set() && rectMap.bottomRight.is_set()
  },
  reset : () => {
    rectMap.topLeft.reset()
    rectMap.bottomRight.reset()
  }
}
let mouse_position = new Point();

const draw_map = async () => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  if ( rectMap.topLeft.is_set() ) {
    if ( !rectMap.bottomRight.is_set() ) {
      console.log("bottom right not set")
      if ( mouse_position.is_set() ) {
        let rect_width = mouse_position.x - rectMap.topLeft.x
        let rect_height = mouse_position.y - rectMap.topLeft.y

        ctx.strokeStyle = "#00FF00"
        ctx.strokeRect(rectMap.topLeft.x,rectMap.topLeft.y,rect_width,rect_height);
      }
    } else {
      let rect_width = rectMap.bottomRight.x - rectMap.topLeft.x
      let rect_height = rectMap.bottomRight.y - rectMap.topLeft.y

      ctx.strokeStyle = "#00FFFF"
      ctx.strokeRect(rectMap.topLeft.x,rectMap.topLeft.y,rect_width,rect_height);

      // drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)
      video_canvas_ctx.clearRect(0,0, video_canvas.width, video_canvas.height);
      video_canvas_ctx.drawImage(
        video, 
        rectMap.topLeft.x, rectMap.topLeft.y, rect_width, rect_height,
        0,0,rect_width,rect_height);
    }
  }

  requestAnimationFrame(draw_map)
}
requestAnimationFrame(draw_map)

canvas.addEventListener("mousedown", (e) => {
  if ( !rectMap.topLeft.is_set() ) {
    rectMap.topLeft = fixCoordinate(e.clientX, e.clientY)
  } else {
    if ( !rectMap.bottomRight.is_set() ) {
      rectMap.bottomRight = fixCoordinate(e.clientX, e.clientY)
      video_canvas.className = "visible"
    }
  }
})

canvas.addEventListener("mousemove", (e) => {
  mouse_position = fixCoordinate(e.clientX, e.clientY)
})

canvas.addEventListener("mouseout", (e) => {
  mouse_position.reset()
})

canvas.addEventListener("dblclick", (e) => {
  rectMap.reset()
  video_canvas.className = ""
})

/////////////////
/////////////////
/////////////////

async function main(){
  //video
  const cams = await getCams()
  loadCam(cams[0])
  video.addEventListener('loadeddata', loadModel)
}
main()
