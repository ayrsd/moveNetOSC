const COORDINATE_PARAMS_NAME = "name"
const COORDINATE_PARAMS_BUNDLED_XY = "bundled_xy"

////////////////////////
// HUMAN : Set your scenes here
////////////////////////
exports.scenes = {
  "cero" : {
    "pose_0_left_wrist" : {
        [COORDINATE_PARAMS_NAME] : "cero_left",
        [COORDINATE_PARAMS_BUNDLED_XY] : true
      },
  },
  "primera" : {
     "pose_0_left_wrist" : {
      [COORDINATE_PARAMS_NAME] : "primera_left",
      [COORDINATE_PARAMS_BUNDLED_XY] : true
     },

     "pose_0_right_wrist" : {
      [COORDINATE_PARAMS_NAME]: "primera_right",
      [COORDINATE_PARAMS_BUNDLED_XY] : false
    }, 
    
     "pose_0_nose" : {
      [COORDINATE_PARAMS_NAME]: "primera_nose",
      [COORDINATE_PARAMS_BUNDLED_XY] : false
    }, 

    "pose_1_nose" : {
      [COORDINATE_PARAMS_NAME]: "primera_nose_2",
      [COORDINATE_PARAMS_BUNDLED_XY] : false
    }, 

    "pose_1_left_wrist" : {
      [COORDINATE_PARAMS_NAME] : "primera_left_2",
      [COORDINATE_PARAMS_BUNDLED_XY] : true
     },
  },
  "segunda" : {
    "pose_0_nose" : {
      [COORDINATE_PARAMS_NAME] : "segunda_nose",
      [COORDINATE_PARAMS_BUNDLED_XY] : true
    },

    "pose_0_left_wrist" : {
      [COORDINATE_PARAMS_NAME] : "segunda_left_wrist",
      [COORDINATE_PARAMS_BUNDLED_XY] : false
    },

    "pose_1_nose" : {
      [COORDINATE_PARAMS_NAME] : "segunda_nose_2",
      [COORDINATE_PARAMS_BUNDLED_XY] : true
    },

    "pose_1_left_wrist" : {
      [COORDINATE_PARAMS_NAME] : "segunda_left_wrist_2",
      [COORDINATE_PARAMS_BUNDLED_XY] : false
    },
  },
}
